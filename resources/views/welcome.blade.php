<!DOCTYPE html>
<html>
    <head>
        <script src="//fb.me/react-0.14.3.js"></script>
        <script src="//fb.me/react-dom-0.14.3.js"></script>
        <script src="//fb.me/JSXTransformer-0.12.2.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <style type="text/css">
            .hidden {
                display:none !important;
            }
            .nav--go-back {
                display: inline-block;
                padding-bottom: 20px;
            }
            .user-list--item {
                border: thin solid black;
                margin-bottom: 10px;
            }
            .user--pic {
                width: 50px;
                height: 50px;
                vertical-align: middle;
            }
            .user--pic.large {
                width: 100px;
                height: 100px;
                vertical-align: middle;
            }
        </style>
        <meta charset="utf-8">
        <title>Ethode Lunch and Learn: ReactJS 101</title>
    </head>
    <body>
        <div id="app" class="container"/>
    </body>
    <script type="text/jsx">

    // Create the App component
    var App = React.createClass({
        getInitialState: function() {
            var initialPage = {
                back: "false",
                pageName: "User List",
                view: <UserListPage users={this.props.users} onUserClick={this.viewUserDetails} />
            };
            return {
                users: [],
                currentPage: initialPage,
                previousPage: initialPage
            }
        },
        onBackClick: function() {
            this.setState({
                currentPage: this.state.previousPage,
                previousPage: this.state.currentPage
            });
        },
        viewUserDetails: function(user) {
            this.setState({
                currentPage: {
                    back: "true",
                    pageName: "User Details",
                    view: <UserDetailsPage user={user} />
                },
                previousPage: this.state.currentPage            
            });
        },
        render: function() {
            return <div>
                <h1>{this.state.currentPage.pageName}</h1>
                <a href="#" className={"nav--go-back icon icon-left-nav pull-left" + (this.state.currentPage.back==="true"?"":" hidden")} onClick={this.onBackClick}>Back</a>
                {this.state.currentPage.view}
            </div>
        }
    });

    var Row = React.createClass({
        render: function() {
            return (
                <div className="row">
                    {this.props.children}
                </div>
            )
        }
    })

//////////////////////////////
// USER LIST
//////////////////////////////
    var UserList = React.createClass({
        render: function () {
            var onUserClick = this.props.onUserClick;
            var users = this.props.users.map(function (user) {

                // key is required by react when mapping lists
                // this is used internally to keep track of each unique
                // item in the list
                return (
                    <UserListItem key={user.id} user={user} onUserClick={onUserClick}/>
                );
            });
            return (
                <ul className="user-list list-unstyled">
                    {users}
                </ul>
            );
        }
    });

    var UserListItem = React.createClass({

        // This is called whenever someone clicks on the user link
        // Events flow out -- this will travel up to parent components
        // to be handled
        onUserClick: function (event) {
            // Commment this out to handle routing manually
            //event.preventDefault();
            this.props.onUserClick(this.props.user);
        },
        render: function () {
            return (
                <li className="user-list--item">
                    <Row>
                        <a href={"#users/" + this.props.user.id} onClick={this.onUserClick}>
                            <div className="col-md-1">
                                <img className="user--pic" src={this.props.user.pic}/>
                            </div>
                            <div className="col-md-11">
                                <span className="user--name">{this.props.user.firstName} {this.props.user.lastName}</span>
                            </div>
                        </a>
                    </Row>
                </li>
            );
        }
    });

    var UserListPage = React.createClass({
        render: function () {
            return <Row>
                <div className="page--user-list col-md-12">
                        <UserList users={this.props.users} onUserClick={this.props.onUserClick}/>
                </div>
            </Row>
        }
    });

//////////////////////////////
// USER DETAILS
//////////////////////////////
    var UserDetails = React.createClass({
        
        render: function () {
            var specialties = this.props.user.specialties.map(function (specialty, index) {

                // key is required by react when mapping lists
                // this is used internally to keep track of each unique
                // item in the list
                return (
                    <li key={index} className="user--speciality {index}">{specialty}</li>
                );
            });
            return (
                <div className="user-details">
                    <Row>
                        <div className="col-md-6">
                            <img className="user--pic large" src={this.props.user.pic} />
                        </div>
                        <div className="col-md-6">
                            <h3 className="col user--name">{this.props.user.firstName} {this.props.user.lastName}</h3>
                            <h5 className="col user--title">{this.props.user.title}</h5>
                        </div>
                    </Row>
                    <Row>
                        <h2 className="user--phone">{this.props.user.phone}</h2>
                    </Row>
                    <Row>
                        <h4>Specialties</h4>
                        <ul className="user--specialties list-unstyled">
                            {specialties}
                        </ul>
                    </Row>
                </div>
            );
        }
    });

    var UserDetailsPage = React.createClass({
        render: function () {
            return <Row>
                <div className="page--user-details col-md-12">
                        <UserDetails user={this.props.user} />
                </div>
            </Row>
        }
    });

    // Add the component to the #app div
    var users = [
        {
            id: 1,
            firstName: "Jason",
            lastName: "Ables",
            title: "Developer",
            phone: "(706) 536-5041",
            specialties: [
                "PHP",
                "Javascript",
                "MongoDB"
            ],
            pic:"//lh3.googleusercontent.com/-xWArPALE7Cw/AAAAAAAAAAI/AAAAAAAAAAA/AMW9IgdKm3oDdHuPaMMqEbjq3bwsbkpbcg/s192-c-mo/photo.jpg"
        },
        {
            id: 2,
            firstName: "Beavis",
            lastName: "Jones",
            title: "Developer",
            phone: "(555) 867-5309",
            specialties: [
                "Java",
                "Redis",
                "MongoDB"
            ],
            pic:"//slm-assets0.secondlife.com/assets/7481372/lightbox/beavis_cornholio_bunghole.jpg?1365185705"
        },
    ];

    ReactDOM.render(<App users={users}/>, 
        document.getElementById('app')
    );
    </script>
</html>
